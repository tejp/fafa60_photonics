import math
import matplotlib.pyplot as plt

wl = 632e-9
b = 3e-6
d = 1e-5
L = 1.5
K = 100
B_screen = 2
h1 = -5e-6
h2 = 5e-6

res = int(1e4)

ps = [p / res for p in range(0, res + 1)]

def theta(p, h): return math.atan(math.fabs(p - h)/L)

#ger summan av fasvektorerna fran spalt h i punkten p
def amp_vector(theta):
    sum_i = 0
    sum_r = 0
    alpha = b*math.sin(theta)*2*math.pi/(K*wl)
    for k in range(0, K):
        sum_i += math.sin(k*alpha)
        sum_r += math.cos(k*alpha)
    return (sum_i, sum_r)


def calc_I(i, r):
    h_length = math.sqrt(i**2 + r**2)   #pythagoras
    return h_length**2                  #amplitude length squared

def normalize(ps_Is):
    ps, Is = zip(*ps_Is)
    max_I = max(Is)
    Is = [I / max_I for I in Is]
    return zip(ps, Is)



def a():
    ps_Is = []
    for p in ps:
        #slit1
        t = theta(p, h1)
        v1 = amp_vector(t)
        #slit2
        t = theta(p, h2)
        v2 = amp_vector(t)
        #Intensity from sum of vectors from both slits
        #v_sum_I = calc_I(v1[0] + v2[0], v1[1] + v2[1])
        v_sum_I = calc_I(v1[0], v1[1])
        ps_Is.append((p, v_sum_I))
    ps_Is = list(normalize(ps_Is))
    new = list(map(lambda x: (-x[0], x[1]), reversed(ps_Is.copy()))) + ps_Is
    plt.plot(*zip(*new), color = 'red')
    plt.xlabel('p [m]')
    plt.ylabel('Intensity')
    plt.show()

def bb():
    #Intensitetsfördelning: I(Theta) = I_0(sin(Beta)/Beta)**2 *
    # (sin(N*gamma)/sin(gamma))**2
    #Beta = (pi * b / wl) * sin(Theta), gamma = (pi * d / wl) * sin(Theta)
    ps_Is = []
    for p in ps:
        t = theta(p, 0)#höjden mitt emellan spalterna
        if t != 0:
            beta = (math.pi * b / wl) * math.sin(t)
            gamma = (math.pi * d / wl) * math.sin(t)
            I = (math.sin(beta)/beta)**2 * (math.sin(2*gamma)/math.sin(gamma))**2
            ps_Is.append((p, I))
    ps_Is = list(normalize(ps_Is))
    new = list(map(lambda x: (-x[0], x[1]), reversed(ps_Is.copy()))) + ps_Is
    plt.plot(*zip(*new), color = 'blue')
    plt.xlabel('p [m]')
    plt.ylabel('Intensity')
    plt.show()

def c():
    #Intensitetsfördelning: I(Theta) = I_0(sin(Beta)/Beta)**2 
    #Beta = (pi * b / wl) * sin(Theta)
    ps_Is = []
    for p in ps:
        t = theta(p, 0)
        if t != 0:
            beta = (math.pi * b / wl) * math.sin(t)
            I = (math.sin(beta)/beta)**2
            ps_Is.append((p, I))
    ps_Is = list(normalize(ps_Is))
    new = list(map(lambda x: (-x[0], x[1]), reversed(ps_Is.copy()))) + ps_Is
    plt.plot(*zip(*new), color = 'green')
    plt.xlabel('p [m]')
    plt.ylabel('Intensity')
    plt.show()




def run():
    a()
    bb()
    c()
    plt.xlabel('p [m]')
    plt.ylabel('Intensity')
    plt.show()

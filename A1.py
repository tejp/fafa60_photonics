import math
import matplotlib.pyplot as plt

hs = [h / 1000000 for h in range(1, 75001)]

def n_bk7(wl):
    a1 = 2.271176
    a2 = -9.700709e-3
    a3 = 0.0110971
    a4 = 4.622809e-5
    a5 = 1.616105e-5
    a6 = -8.285043e-7

    return math.sqrt(a1 + a2*wl**2 + a3*wl**-2 + a4*wl**-4 + a5*wl**-6 + a6*wl**-8)

def calc_f(h, n1=1, n2=n_bk7(0.55), R=0.1):
    a1 = math.asin(h / R)
    sin_a2 = n1 * h / (R * n2)
    a2 = math.asin(sin_a2)

    return R + R * sin_a2 / math.sin(a1 - a2)

def max_h(h_fs, f0, d=0.01):
    *_, last = filter(lambda h_f: f0 - h_f[1] <= d, h_fs)
    return last[0]

def a():
    fs = [calc_f(h) for h in hs]
    plt.plot(hs, fs, color='green')
    plt.xlabel('h [m]')
    plt.ylabel('f(h) [m]')
    plt.show()

def b():
    h_fs = ((h, calc_f(h)) for h in hs)
    print(max_h(h_fs, next(h_fs)[1]))

def c():
    h_fs_400 = [(h, calc_f(h, n2=n_bk7(0.40))) for h in hs]
    h_fs_550 = [(h, calc_f(h, n2=n_bk7(0.55))) for h in hs]
    h_fs_700 = [(h, calc_f(h, n2=n_bk7(0.70))) for h in hs]
    f0 = max(h_fs_400[0][1], h_fs_550[0][1], h_fs_700[0][1])
    h = min(max_h(h_fs, f0) for h_fs in [h_fs_400, h_fs_550, h_fs_700])
    print(h)
    plt.plot(*zip(*h_fs_400), color='violet')
    plt.plot(*zip(*h_fs_550), color='green')
    plt.plot(*zip(*h_fs_700), color='red')
    plt.xlabel('h [m]')
    plt.ylabel('f(h) [m]')
    plt.show()
